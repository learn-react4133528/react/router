import { Route, Routes } from 'react-router-dom';
import './App.css';
import { Home } from './components/Home.jsx';
// import {About} from "./components/About.jsx";
import { lazy, Suspense } from 'react';
import { Admin } from './components/Admin.jsx';
import { AuthProvider } from './components/auth.jsx';
import { FeaturedProducts } from './components/FeaturedProducts.jsx';
import { Login } from './components/Login.jsx';
import { Navbar } from './components/navbar.jsx';
import { Newproducts } from './components/Newproducts.jsx';
import { NoMatch } from './components/NoMatch.jsx';
import { OrderSummary } from './components/OrderSummary.jsx';
import { Products } from './components/Products.jsx';
import { Profile } from './components/Profile.jsx';
import { RequireAuth } from './components/RequireAuth.jsx';
import { UserDetails } from './components/UserDetails.jsx';
import { Users } from './components/Users.jsx';
const LazyAbout = lazy(() => import('./components/About.jsx'));

function App() {
  return (
    <AuthProvider>
      <Navbar />
      <Routes>
        <Route path={'/'} element={<Home />} />
        <Route
          path={'about'}
          element={
            <Suspense fallback={'Loading...'}>
              <LazyAbout />
            </Suspense>
          }
        />
        <Route path={'order-summary'} element={<OrderSummary />} />
        <Route path={'products'} element={<Products />}>
          <Route index element={<div>No page is selected.</div>} />
          <Route path={'featured'} element={<FeaturedProducts />} />
          <Route path={'new'} element={<Newproducts />} />
        </Route>
        <Route path={'users'} element={<Users />}>
          <Route path={':userId'} element={<UserDetails />}></Route>
          <Route path={'admin'} element={<Admin />}></Route>
        </Route>
        <Route
          path={'profile'}
          element={
            <RequireAuth>
              <Profile />
            </RequireAuth>
          }
        />
        <Route path={'login'} element={<Login />} />
        <Route path={'*'} element={<NoMatch />} />
      </Routes>
    </AuthProvider>
  );
}

export default App;
