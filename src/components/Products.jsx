import {Link, Outlet} from "react-router-dom";
import {useState} from "react";

export const Products = () => {
    const [id, setId] = useState('');

    return (
        <>
            <div>
                <input onChange={(e) => setId(e.target.value)} type="search" placeholder={'search products'}/>
            </div>
            <nav>
                <Link to={'featured'}>Featured</Link>
                <Link to={'new'}>New Products</Link>
            </nav>
            <Outlet />
        </>
    )
};