import {useParams} from "react-router-dom";

export const UserDetails = () => {
    const params = useParams();
    return (
        <div>
            Details about user {params.userId}
        </div>
    )
};